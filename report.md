| **Table Progeress report** |

| Shakhzod Karimov      | Stage   | Start date | End date | Comment |
|---------|---------|------------|----------|---------|
| 1. Task Clarification  | Recieve and analyze the task | 13 may 23: 00   |   14 may 9:00 | Many misundesrtandinga  | 
| 2. Task Clarification  | Clarify details and requirements with the mentor | 14 may 9:20    |   |  We haven't clarified the details yet | 
| 3. Task Clarification  | Propose a 10-week work plan for course project development | 14 may 10:30     |   |   | 
| 4. Task Clarification  | Create a progress report and place it in your repository | 14 may   11:00   |   |   | 
| --- | --- | --- | ---| --- |
| 1. Analysis  |  Study the applied area by the individual task |  14 may 12:00     |   |   | 
| 2. Analysis  |  Create a progress report and place it in your repository | 14 may 14:00   |   |   | 
| 3. Analysis  |  Define and describe the basic functionality that needs to be implemented first (MVP—minimum viable product) | 14 may 15:30     |   |   | 
| 4. Analysis  |  Describe additional functionality for improving usability, security, performance, etc. | 14 may 16:00     |   |   | 
| 5. Analysis  | Describe any advanced functionality that might be useful in the future | 14 may 18:00     |   |   | 
| --- | --- | --- | ---| --- |
| 1. Use Cases  | Describe several options for using the program (how the program should behave from the user's point of view) however you prefer. | 14 may 19:30    |   |   |
